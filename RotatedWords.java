import java.util.Scanner;

/**
 * This program checks if word1 is rotation of word2 (rotate left: "abc --> "bca").
 * @version 1.0 4.10.2017
 * @author CostelRo
 */


public class RotatedWords
{
    public static void main(String[] args)
    {
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter the first word: ");
        String userWord1 = userInput.nextLine();
        System.out.print("Enter the second word: ");
        String userWord2 = userInput.nextLine();

        userInput.close();

        String[] userWords = {userWord1, userWord2};

        System.out.println("\n" + ">> " + RotatedWords.showResult(userWords));
    }

    static String showResult(String[] words)
    {
        if (words.length < 2)
        {
            return "Words are missing...";
        }

        String word1 = words[0];
        String word2 = words[1];

        if ( word1.isEmpty() || word2.isEmpty() )
        {
            return "Word is missing...";
        }
        else if (word1.length() != word2.length())
        {
            return "The words have different length, so can't be rotated (:";
        }
        else if ( word1.equals(word2) )
        {
            return "The words are identical, so are rotated :)";
        }
        else
        {
            return checkRotation(word1, word2);
        }
    }


    private static int counterCheckRotation = 0;

    private static String checkRotation(String word1, String word2)
    {
        String testingWord1Rotated = word1.substring(1, word1.length()) + word1.charAt(0);
        counterCheckRotation++;

        if ( (counterCheckRotation < word1.length())
            && testingWord1Rotated.equals(word2) )
        {
            return "The 2 words are rotated :)";
        }
        else if ( counterCheckRotation < word1.length() )
        {
            return checkRotation(testingWord1Rotated, word2);
        }
        else
        {
            return "The 2 words aren't rotated (:";
        }
    }
}
