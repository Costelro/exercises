import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests created by CostelRo.
 */


public class RotatedWordsTest
{
    @Test
    public void returnsCorrectlyForTwoRotatedWords() throws Exception
    {
        String word1 = "ab";
        String word2 = "ba";
        String[] testedWordsArray = {word1, word2};

        String runtimeResult = RotatedWords.showResult(testedWordsArray);

        String expectedResult = "The 2 words are rotated :)";
        assertEquals(expectedResult, runtimeResult);
    }

    @Test
    public void returnsCorrectlyForTwoUnrotatedWords() throws Exception
    {
        String word1 = "123";
        String word2 = "311";
        String[] testedWordsArray = {word1, word2};

        String runtimeResult = RotatedWords.showResult(testedWordsArray);

        String expectedResult = "The 2 words aren't rotated (:";
        assertEquals(expectedResult, runtimeResult);
    }

    @Test
    public void returnsCorrectlyForWordsWithDifferentLength() throws Exception
    {
        String word1 = "123";
        String word2 = "123456";
        String[] testedWordsArray = {word1, word2};

        String runtimeResult = RotatedWords.showResult(testedWordsArray);

        String expectedResult = "The words have different length, so can't be rotated (:";
        assertEquals(expectedResult, runtimeResult);
    }

    @Test
    public void returnsCorrectlyWithOneEmptyWord() throws Exception
    {
        String word1 = "123";
        String[] testedWordsArray1 = {word1, ""};
        String[] testedWordsArray2 = {"", word1};

        String runtimeResult1 = RotatedWords.showResult(testedWordsArray1);
        String runtimeResult2 = RotatedWords.showResult(testedWordsArray2);

        String expectedResult = "Word is missing...";
        assertEquals(expectedResult, runtimeResult1);
        assertEquals(expectedResult, runtimeResult2);
    }

    @Test
    public void returnsCorrectlyWithLessThenTwoWords() throws Exception
    {
        String[] testedWordsArray = {"abc"};

        String runtimeResult = RotatedWords.showResult(testedWordsArray);

        String expectedResult = "Words are missing...";
        assertEquals(expectedResult, runtimeResult);
    }
}