import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests created by CostelRo.
 */


public class FancyBoxForWordsTest
{
    @Test
    public void displayCorrectlyUsingProperStringOfWords() throws Exception
    {
        String testString = "String to be displayed well.";
        String testFancySymbol = "*";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("ok", runtimeResult);
    }

    @Test
    public void displayCorrectlyUsingStringWithTooManySpaces() throws Exception
    {
        String testString = "    String    to be   displayed well. ";
        String testFancySymbol = "*";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("ok", runtimeResult);
    }

    @Test
    public void displayCorrectlyUsingFancySymbolWithTooManySpaces() throws Exception
    {
        String testString = "String to be displayed well.";
        String testFancySymbol = " *  ";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("ok", runtimeResult);
    }

    @Test
    public void displayCorrectlyUsingLongFancySymbol() throws Exception
    {
        String testString = "String to be displayed well.";
        String testFancySymbol = "*12345";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("ok", runtimeResult);
    }

    @Test
    public void stopIfStringIsEmpty() throws Exception
    {
        String testString = "";
        String testFancySymbol = "*";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("Program end: Input string(s) empty.", runtimeResult);
    }

    @Test
    public void stopIfFancySymbolIsEmpty() throws Exception
    {
        String testString = "String to be displayed well.";
        String testFancySymbol = "";

        String runtimeResult = FancyBoxForWords.processUserInput(testString, testFancySymbol);

        assertEquals("Program end: Input string(s) empty.", runtimeResult);
    }

    @Test
    public void stopIfStringOrFancySymbolOrBothAreNull() throws Exception
    {
        String testString1 = null;
        String testString2 = "abc";
        String testFancySymbol1 = null;
        String testFancySymbol2 = "*";

        String runtimeResult1 = FancyBoxForWords.processUserInput(testString1, testFancySymbol1);
        String runtimeResult2 = FancyBoxForWords.processUserInput(testString1, testFancySymbol2);
        String runtimeResult3 = FancyBoxForWords.processUserInput(testString2, testFancySymbol1);

        assertEquals("Program end: Input string(s) empty.", runtimeResult1);
        assertEquals("Program end: Input string(s) empty.", runtimeResult2);
        assertEquals("Program end: Input string(s) empty.", runtimeResult3);
    }
}
