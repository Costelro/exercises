import java.util.Scanner;

/**
 * This program displays a group of words into a fancy box, each word on a line.
 * @version 1.0 2.10.2017
 * @author CostelRo
 */


public class FancyBoxForWords
{
    private static int maxWordLength(String[] sourceWords)
    {
        int longestWord = 0;
        for (String word : sourceWords)
        {
            if ( longestWord < word.length() )
            {
                longestWord = word.length();
            }
        }
        return longestWord;
    }

    private static String createTableEnding(String[] sourceWords, String symbol)
    {
        int headerLength = maxWordLength(sourceWords) + 4;

        StringBuilder fancyBuilder = new StringBuilder();
        for (int i = 0; i < headerLength; i++)
        {
            fancyBuilder.append(symbol);
        }

        return fancyBuilder.toString();
    }

    private static void createFancyTable(String[] sourceWords, String symbol)
    {
        System.out.println(createTableEnding(sourceWords, symbol));

        int rowLength = maxWordLength(sourceWords);
        for (String word : sourceWords)
        {
            if (word.isEmpty())
            {
                continue;
            }

            String fancyString = String.format("%1$-" + rowLength + "s", word);
            System.out.println( symbol + " " + fancyString + " " + symbol );
        }

        System.out.println(createTableEnding(sourceWords, symbol));
    }

    public static String processUserInput(String sourceText, String sourceFancySymbol)
    {
        if ( sourceText == null
            || sourceFancySymbol == null
            || sourceText.isEmpty()
            || sourceFancySymbol.isEmpty() )
        {
            String result = "Program end: Input string(s) empty.";
            System.out.println(result);
            return result;
        }

        String[] sourceWords = sourceText.trim().split(" ");
        String fancySymbol = sourceFancySymbol.trim().substring(0, 1);

        createFancyTable(sourceWords, fancySymbol);

        return "ok";
    }

    public static void main(String[] args)
    {
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter string of words: ");
        String sourceText = userInput.nextLine();
        System.out.print("Now enter decorative symbol (1 character): ");
        String sourceFancySymbol = userInput.nextLine();
        userInput.close();

        processUserInput(sourceText, sourceFancySymbol);
    }
}
